import horovod.torch as hvd
import torch
from torch import nn
from torchvision import transforms, datasets

# Initialize Horovod
hvd.init()

cuda_available = torch.cuda.is_available()
print("Cuda:", cuda_available)
# Pin GPU to be used to process local rank (one GPU per process)
if cuda_available:
    torch.cuda.set_device(hvd.local_rank())

transform = transforms.Compose([transforms.ToTensor(), ])
# Define dataset...
train_dataset = datasets.MNIST(root='./data', download=True, train=True, transform=transform)

# Partition dataset among workers using DistributedSampler
train_sampler = torch.utils.data.distributed.DistributedSampler(
    train_dataset, num_replicas=hvd.size(), rank=hvd.rank())

train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=128, sampler=train_sampler)


# Build model...
class Flatten(nn.Module):
    def forward(self, x):
        batch_size = x.shape[0]
        return x.view(batch_size, -1)


model = nn.Sequential(
    Flatten(),
    nn.Linear(784, 128),
    nn.ReLU(),
    nn.Linear(128, 64),
    nn.ReLU(),
    nn.Linear(64, 10)
)
if cuda_available:
    model.cuda()
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.1)

# Add Horovod Distributed Optimizer
optimizer = hvd.DistributedOptimizer(optimizer, named_parameters=model.named_parameters())

# Broadcast parameters from rank 0 to all other processes.
hvd.broadcast_parameters(model.state_dict(), root_rank=0)

for epoch in range(100):
    for batch_idx, (data, target) in enumerate(train_loader):
        if cuda_available:
            data, target = data.cuda(), target.cuda()
        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % 1 == 0:
            print('Train Epoch: {} [{}/{}]\tLoss: {}'.format(
                epoch, batch_idx * len(data), len(train_sampler), loss.item()))
