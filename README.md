# Pytorch + Horovod
Code study sample that parallelize on more machines the MNIST train.

## Local
```bash
pip install -r requirements
horovodrun -np 1 -H localhost:4 python main.py
```

## Docker
* run one container
```bash
docker-compose -f docker-compose_alone.yml up 
```
* run two container (wip to do enable ssh on slave)
```bash
docker-compose up
```
